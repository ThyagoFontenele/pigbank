import React from 'react'
import Header from '../../components/Header/Header'
import './Cotacao.css'
function Cotacao() {
    return (
        <div>
            <Header/>
            <nav className="field">
                <ul>
                    <li className="retangulo-da-esquerda"></li>
                    <li className='cotacao-texto'>Cotação moedas</li>
                    <li className="retangulo-da-direita"></li>
                </ul>
            </nav>
            
        </div>
    )
}

export default Cotacao
