import React from 'react'
import Header from '../../components/Header/Header'
import { Link } from 'react-router-dom'
import './HomePage.css'
import '../../components/Button/Botao.css'


import PigHomePageImg from '../../images/pighome.png'
import Circulos from '../../images/circles.png'
import Pessoas from '../../images/pessoas.png'
function HomePage() {
    return (
        
        <>
            <Header/>
            <div className="div1">
                <img src={PigHomePageImg}  alt="porco-cofre" className="img-porquin"  />
                <nav>
                    <h1>A mais nova alternativa de banco digital chegou!</h1>
                    <p>Feito para caber no seu bolso e otimizar seu tempo. O <strong>PigBank</strong> veio pra ficar</p>
                   <Link to="/em-construcao" className="abra-sua-conta"><button >Abra a sua conta</button></Link> 
                </nav>
            </div>
            
            <div>
                <nav className="fique">
                    <div className="retangulo-esquerda"></div>
                    <div className="retangulo-direita"> </div>
                    <h1 className="fique-por-dentro">Fique por dentro!</h1>
                    <h2 className="texto-contrario">Ao contrário do ditado popular, por aqui, quem se mistura com porco não come farelo! Conheça nossa plataforma exclusivamente dedicada para ampliar o seu patrimônio.</h2>
                </nav>
               
            </div>
            
            <nav>
                <div>
                    <img src={Circulos} alt="circles" className="img-circulos" />
                    <img src={Pessoas} alt="pessoas" className="img-pessoas" />
                </div>
            </nav>
            <button className="cotacao-moedas">Cotação Moedas</button>
           
        </>
    )
}

export default HomePage
