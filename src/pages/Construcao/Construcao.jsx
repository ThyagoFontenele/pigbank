import React from 'react'
import Header from '../../components/Header/Header'
import { Link } from 'react-router-dom'
import PigBuild from '../../images/pigbuild.svg'
import './Construcao.css'
function Construcao() {
    return (
        <div>
            <Header/>
            <div className="field-building">
                <h1>Em construção</h1>
                <img src={PigBuild} alt="porco-guindaste" />
                <p>Parece que essa página ainda não foi implementada... Tente novamente mais tarde!</p>
                <Link to="/"><button>VOLTAR</button></Link>
            </div>
        </div>
    )
}

export default Construcao
