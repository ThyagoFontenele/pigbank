import React from 'react'
import Header from '../../components/Header/Header'
import PigConfuso from '../../images/pig-confuso.svg'
import { Link } from 'react-router-dom'
import './NotFound.css'
function NotFound() {
    return (
        <>
            <Header />
            <div className="field-404">
                <h1>Erro 404</h1>
                <img src={PigConfuso} alt="pigconfuso" className="PigConfuso" />
                <p>Ops! Parece que o endereço que você digitou está incorreto...</p>
                <Link to="/"><button>VOLTAR</button></Link>
            </div>
        </>
    )
}

export default NotFound
