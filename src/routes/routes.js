import React from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import HomePage from '../pages/HomePage/HomePage'
import Cotacao from '../pages/Cotacao/Cotacao'
import NotFound from '../pages/NotFound/NotFound'
import Construcao from '../pages/Construcao/Construcao'
function Routes(){
    return(
        <Router>
            <Switch>
              
                <Route exact path="/">
                    <HomePage/>
                </Route>

                <Route exact path="/cotacao-moedas">
                    <Cotacao/>
                </Route>
                <Route exact path="/em-construcao">
                    <Construcao/>
                </Route>
                <Route exact path="/404">
                    <NotFound/>
                </Route>
                <Route>
                    <Redirect to="/404"/>
                </Route>

            </Switch>
        </Router>
    )
}

export default Routes