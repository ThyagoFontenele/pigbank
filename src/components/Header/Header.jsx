import React from 'react'
import './Header.css'
import { Link } from 'react-router-dom'

function Header() {
    return (
        <>
            <header>
                <Link to="/" className="Pigbank"><h1>Pigbank</h1></Link>
                <Link to="/cotacao-moedas"><button>Cotação Moedas</button></Link>
                <Link to="/em-construcao"><button className="botao-diferente">Acessar</button></Link>
            </header>
           
        </>
    )
}

export default Header
